# README #

Sunspot classification using Convolutional Neural Networks

### What is this repository for? ###

* SS project

### How do I get set up? ###

* Create a new environment (conda, virtualenv, pyenv)
* pip install -r requirements.txt
* To run the tests `pytest `

### Contribution guidelines ###

* Copy the hooks/pre-commit file to you local .git/hooks/ `cp hooks/pre-commit .git/hooks`
* This will run the relevant style checks and runt tests before you can commit